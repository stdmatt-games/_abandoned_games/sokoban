//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Sokoban.js                                                    //
//  Project   : js_demos                                                      //
//  Date      : Aug 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//   Just a simple sokoban game...                                            //
//---------------------------------------------------------------------------~//

const image = document.getElementById('sprite_sheet_sokoban');


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
const CHAR_WALL                  = '#';
const CHAR_PLAYER                = '@';
const CHAR_PLAYER_ON_GOAL_SQUARE = '+';
const CHAR_BOX                   = '$';
const CHAR_BOX_ON_GOAL_SQUARE    = '*';
const CHAR_GOAL_SQUARE           = '.';
const CHAR_FLOOR                 = ' ';

const SPRITE_SIZE = 64;

const GAME_STATE_CONTINUE = 0;
const GAME_STATE_VICTORY  = 1;

const MOVE_PLAYER_DURATION = 0.2;
const MOVE_BOX_DURATION    = 0.2;


//----------------------------------------------------------------------------//
// Level element         Character      ASCII Code                            //
// Wall                  #              0x23                                  //
// Player                @              0x40                                  //
// Player on goal square +              0x2b                                  //
// Box                   $              0x24                                  //
// Box on goal square    *              0x2a                                  //
// Goal square           .              0x2e                                  //
// Floor                 (Space)        0x20                                  //
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function MakeSpriteName(base, i)
{
    let frame_name = base + "_" +
          ((i < 10) ? "0" : "") +
           i + ".png";
    return frame_name;
}

//------------------------------------------------------------------------------
function DrawTexture(name)
{
    let info = SS.sprites[name];

    let sx = info["-x"];
    let sy = info["-y"];
    let sw = info["-width"];
    let sh = info["-height"];

    let dx = (SPRITE_SIZE/2) - (sw/2);
    let dy = (SPRITE_SIZE/2) - (sh/2);

    Canvas_DrawTexture(
        image,
        sx, sy, sw, sh,
        dx, dy, sw, sh
    );
}

function Coord2Pos(c)
{
    return Vector_Create(
        Math_Int(c.x * SPRITE_SIZE),
        Math_Int(c.y * SPRITE_SIZE)
    );
}
function Pos2Coord(p)
{
    return Vector_Create(
        Math_Int(p.x / SPRITE_SIZE),
        Math_Int(p.y / SPRITE_SIZE),
    );
}

//----------------------------------------------------------------------------//
// Types                                                                      //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
class SokObj
{
    constructor(coord)
    {
        this.startCoord  = coord;
        this.targetCoord = coord;

        this.position = Coord2Pos(coord);

        this.moveDuration   = 0;
        this.moveTime       = 0;
        this.isMoving       = false;
    }


    moveTo(coord, timeToMove)
    {
        if(this.isMoving) {
            return;
        }

        this.startCoord  = this.targetCoord;;
        this.targetCoord = coord;

        this.moveDuration = timeToMove;
        this.moveTime     = 0;
        this.isMoving     = true;
    }

    update(dt)
    {
        if(!this.isMoving) {
            return;
        }

        this.moveTime = Math_Clamp(0, this.moveDuration, this.moveTime + dt);;
        if(this.moveTime >= this.moveDuration) {
            this.isMoving   = false;
            this.position   = Coord2Pos(this.targetCoord);
            this.startCoord = this.targetCoord;
        } else {
            let sp = Coord2Pos(this.startCoord);
            let tp = Coord2Pos(this.targetCoord);

            let x = Math_Lerp(sp.x, tp.x, this.moveTime / this.moveDuration);
            let y = Math_Lerp(sp.y, tp.y, this.moveTime / this.moveDuration);

            this.position.x = x;
            this.position.y = y;
        }
    }

}; // class SokObj

//------------------------------------------------------------------------------
class Player extends SokObj
{
    //--------------------------------------------------------------------------
    constructor(coord) {
        super(coord);

        this.rightAnimFrames = [17, 18];
        this.leftAnimFrames  = [20, 21];
        this.upAnimFrames    = [9,  10];
        this.downAnimFrames  = [6,  7];

        this.currFrameTime  = 0;
        this.maxFrameTime   = 0.5;
        this.currFrameIndex = 0;
        this.currAnimFrames = null;

        this.facingDir = null;
        this.changeDirection(Vector_Create(1, 0));
    } // ctor

    //--------------------------------------------------------------------------
    changeDirection(dir)
    {
        this.facingDir = dir;
        if(dir.x == +1) {
            this.currAnimFrames = this.rightAnimFrames;
        } else if(dir.x == -1) {
            this.currAnimFrames = this.leftAnimFrames;
        } else if(dir.y == -1) {
            this.currAnimFrames = this.upAnimFrames;
        } else if(dir.y == +1) {
            this.currAnimFrames = this.downAnimFrames;
        }
    } // changeDirection

    //--------------------------------------------------------------------------
    update(dt)
    {
        super.update(dt);

        //
        // "Walk" animation.
        this.currFrameTime += dt;
        if(this.currFrameTime >= this.maxFrameTime) {
            this.currFrameTime -= this.maxFrameTime;
            this.currFrameIndex = Math_IntMod(
                this.currFrameIndex + 1,
                this.currAnimFrames.length
            );
        }
    } // update

    //--------------------------------------------------------------------------
    draw()
    {
        Canvas_Push();
            let x = Canvas_Edge_Left + this.position.x;
            let y = Canvas_Edge_Top  + this.position.y;

            Canvas_Translate(x, y);

            let i = this.currAnimFrames[this.currFrameIndex];
            DrawTexture(MakeSpriteName("player", i));
        Canvas_Pop()
    } // draw
}; // class Player


//------------------------------------------------------------------------------
class Box extends SokObj
{
    //--------------------------------------------------------------------------
    constructor(coord, isOnGoal = false) {
        super(coord);
        this.isOnGoal = isOnGoal;
    }

    //--------------------------------------------------------------------------
    draw()
    {
        Canvas_Push();
            let x = Canvas_Edge_Left + this.position.x;
            let y = Canvas_Edge_Top  + this.position.y;

            Canvas_Translate(x, y);
            let i = this.isOnGoal && !this.isMoving ? 16 : 6;
            DrawTexture(MakeSpriteName("crate", i));
        Canvas_Pop();
    }
}; // class Box


//------------------------------------------------------------------------------
class Goal extends SokObj
{
    //--------------------------------------------------------------------------
    constructor(coord) {
        super(coord);
    }
}; // class Goal


//------------------------------------------------------------------------------
class SpriteSheet
{
    constructor(ss)
    {
        this.sprites = [];
        let textures = ss["TextureAtlas"]["SubTexture"];
        for(let i = 0; i < textures.length; ++i) {
            let name = textures[i]["-name"];
            this.sprites[name] = textures[i];
        }
    }
}; // class SpriteSheet



//------------------------------------------------------------------------------
class Level
{
    //--------------------------------------------------------------------------
    constructor(lines)
    {
        this.width  = this._findLongestLength(lines);
        this.height = lines.length;

        this.state  = GAME_STATE_CONTINUE;

        this.map    = Array_Create2D(this.height, this.width);
        this.player = null;
        this.boxes  = [];
        this.goals  = [];

        //
        // Parse the level data from lines.
        for(let i = 0; i < this.height; ++i) {
            for(let j = 0; j < this.width; ++j) {
                let c = lines[i][j];
                let p = Vector_Create(j, i);

                // Goal
                if(c == CHAR_GOAL_SQUARE) {
                    this.goals.push(new Goal(p));
                    c = CHAR_FLOOR;
                }

                // Player
                else if(c == CHAR_PLAYER) {
                    this.player = new Player(p);
                    c = CHAR_FLOOR;
                } else if(c == CHAR_PLAYER_ON_GOAL_SQUARE) {
                    this.player = new Player(p);
                    this.goals.push(new Goal(p));
                    c = CHAR_GOAL_SQUARE;
                }

                // Box
                else if(c == CHAR_BOX) {
                    this.boxes.push(new Box(p));
                    c = CHAR_FLOOR;
                } else if(c == CHAR_BOX_ON_GOAL_SQUARE) {
                    this.boxes.push(new Box(p, true));
                    this.goals.push(new Goal(p));
                    c = CHAR_GOAL_SQUARE;
                }

                this.map[i][j] = c;
            }
        }
    } // ctor

    //--------------------------------------------------------------------------
    move(dir)
    {
        if(this.state != GAME_STATE_CONTINUE || this.player.isMoving) {
            return;
        }

        // Always change the looking direction.
        this.player.changeDirection(dir);

        let player_coord = Pos2Coord(this.player.position);
        let dst_coord    = Vector_Add(player_coord, dir);

        if(this._isMovable(dst_coord)) {
            this.player.moveTo(dst_coord, MOVE_PLAYER_DURATION);
        } else if(this._isABox(dst_coord)) {
            let box_dst_coord = Vector_Add(dst_coord, dir);
            if(this._isMovable(box_dst_coord)) {
                this.player.moveTo(dst_coord, MOVE_PLAYER_DURATION);

                let b = this._getBoxAt(dst_coord);
                b.moveTo(box_dst_coord, MOVE_BOX_DURATION);
                b.isOnGoal = this._isAGoal(box_dst_coord);

                if(this._areAllBoxesOnGoal()) {
                    this.state = GAME_STATE_VICTORY;
                }
            }
        }
    } // move

    //--------------------------------------------------------------------------
    _areAllBoxesOnGoal()
    {
        for(let i = 0, len = this.boxes.length; i < len; ++i) {
            if(!this.boxes[i].isOnGoal) {
                return false;
            }
        }

        return true;
    } // _areAllBoxesOnGoal

    //--------------------------------------------------------------------------
    _isABox(v)
    {
        let b = this._getBoxAt(v);
        return b != null;
    } // _isABox

    //--------------------------------------------------------------------------
    _isAGoal(v)
    {
        let g = this._getGoalAt(v);
        return g != null;
    } // _isAGoal

    //--------------------------------------------------------------------------
    _isMovable(v)
    {
        let c = this._getMapAt(v);
        if(c == CHAR_WALL) {
            return false;
        }

        if(this._isABox(v)) {
            return false;
        }

        return true;
    } // _isMovable

    //--------------------------------------------------------------------------
    _getGoalAt(v)
    {
        for(let i = 0, len = this.goals.length; i < len; ++i) {
            if(Vector_Equals(v, this.goals[i].targetCoord)) {
                return this.goals[i];
            }
        }

        return null;
    } // _getGoalAt

    //--------------------------------------------------------------------------
    _getBoxAt(v)
    {
        for(let i = 0, len = this.boxes.length; i < len; ++i) {
            if(Vector_Equals(v, this.boxes[i].targetCoord)) {
                return this.boxes[i];
            }
        }

        return null;
    } // _getBoxAt

    //--------------------------------------------------------------------------
    _getMapAt(v)
    {
        return this.map[v.y][v.x];
    } // _getMapAt

    //--------------------------------------------------------------------------
    update(dt)
    {
        this.player.update(dt);
        for(let i = 0, len = this.boxes.length; i < len; ++i) {
            this.boxes[i].update(dt);
        }
    } // update

    //--------------------------------------------------------------------------
    draw()
    {
        for(let i = 0; i < this.height; ++i) {
            for(let j = 0; j < this.width; ++j) {
                Canvas_Push();
                    // Draw the Floor ALWAYS.
                    let x = Canvas_Edge_Left + (j * SPRITE_SIZE);
                    let y = Canvas_Edge_Top  + (i * SPRITE_SIZE);
                    Canvas_Translate(x, y);

                    DrawTexture("ground_01.png");

                    if(this.map[i][j] == CHAR_WALL) {
                        DrawTexture("block_01.png");
                    }

                Canvas_Pop();
            }
        }

        // Goals
        for(let i = 0, len = this.goals.length; i < len; ++i) {
            Canvas_Push();
                let g = this.goals[i];

                let x = Canvas_Edge_Left + (g.position.x * SPRITE_SIZE);
                let y = Canvas_Edge_Top  + (g.position.y * SPRITE_SIZE);

                Canvas_Translate(x, y);
                DrawTexture("environment_13.png");
            Canvas_Pop();
        }

        // Boxes
        for(let i = 0, len = this.boxes.length; i < len; ++i) {
            this.boxes[i].draw();
        }

        // Player
        this.player.draw();
    } // draw

    //--------------------------------------------------------------------------
    _findLongestLength(lines)
    {
        let ml = 0;
        for(let i = 0; i < lines.length; ++i) {
            if(lines[i].length > ml) {
                ml = lines[i].length;
            }
        }
        return ml;
    } // _findLongestLength

    //--------------------------------------------------------------------------
    ascii()
    {
        let s = "";
        for(let i = 0; i < this.height; ++i) {
            for(let j = 0; j < this.width; ++j) {
                s += this.map[i][j];
            }
            s += "\n";
        }
        return s;
    } // ascii
}; // class Level



//----------------------------------------------------------------------------//
// Global Vars                                                                //
//----------------------------------------------------------------------------//
let level;
let SS;


//----------------------------------------------------------------------------//
// Setup / Draw                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function Preload()
{

}

//------------------------------------------------------------------------------
function Setup()
{
    level = new Level([
        // "#####",
        // "#@$.#",
        // "#####"

        // "#####",
        // "     ",
        // "#@$.#",
        // "     ",
        // "#####"

        // "@    ",
        // "     ",
        // "     ",
        // "     ",
        // "     "

        "#######",
        "#.@ # #",
        "#$* $ #",
        "#   $ #",
        "# ..  #",
        "#  *  #",
        "#######",
    ]);
    SS = new SpriteSheet(SPRITE_SHEET);
    Log(level.ascii());
}


let zoom = 1;
//------------------------------------------------------------------------------
function Draw(dt)
{
    Canvas_ClearWindow("black");
    level.update(dt);

    Canvas_Push();
    // Canvas_Translate(Mouse_World_X, Mouse_World_Y);
    let s = zoom;
    Canvas_Scale(s, s);
    level.draw();
    Canvas_Pop();

}


//----------------------------------------------------------------------------//
// Input                                                                      //
//----------------------------------------------------------------------------//
function KeyDown(code)
{
    let v = Vector_Create(0, 0);
    if(code == KEY_LEFT) {
        v.x = -1;
    } else if(code == KEY_RIGHT) {
        v.x = +1;
    } else if(code == KEY_UP) {
        v.y = -1;
    } else if(code == KEY_DOWN) {
        v.y = +1;
    }

    else if(code == KEY_A) {
        zoom += 0.1;
    } else if(code == KEY_D) {
        zoom -= 0.1;
    }

    if(v.x != 0 || v.y != 0) {
        level.move(v);
    }
}


Canvas_Setup({
    main_title        : "Simple Snake",
    main_date         : "Aug 10, 2019",
    main_version      : "v0.0.1",
    main_instructions : "<br><b>arrow keys</b> to move the snake<br><b>R</b> to start a new game.",
    main_link: "<a href=\"http://stdmatt.com/demos/startfield.html\">More info</a>"
});

//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
